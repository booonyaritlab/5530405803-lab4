package pengjam.boonyarit.lab4;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Player {

	private String name;
	private int bDay;
	private int bMonth;
	private int bYear;
	private double weight;
	private double height;

	public Player(String n, int d, int m, int y, double w, double h) {
		name = n;
		bDay = d;
		bMonth = m;
		bYear = y;
		weight = w;
		height = h;

	}

	public String getName() {
		return name;
	}

	public void setName(String newname) {
		name = newname;
	}

	public int getbDay() {
		return bDay;
	}

	public void setbDay(int newbDay) {
		bDay = newbDay;
	}

	public int getbMonth() {
		return bMonth;
	}

	public void setbMonth(int newbMonth) {
		bMonth = newbMonth;
	}

	public int getbYear() {
		return bYear;
	}

	public void setbYear(int newbYear) {
		bYear = newbYear;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double newweight) {
		weight = newweight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double newheight) {
		height = newheight;
	}

	public String toString() {

		Calendar cal = new GregorianCalendar(bYear, bMonth, bDay);
		Calendar now = new GregorianCalendar();
		int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
				|| (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
						.get

						(Calendar.DAY_OF_MONTH) > now
						.get(Calendar.DAY_OF_MONTH))) {
			res--;
		}

		return "Player [" + name + "  is " + res + " year old, with weight = "
				+ weight + " and height = " + height + "]";
	}

}