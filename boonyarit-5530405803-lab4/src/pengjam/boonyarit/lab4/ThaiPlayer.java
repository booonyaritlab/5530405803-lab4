package pengjam.boonyarit.lab4;

public class ThaiPlayer extends Player {

	private String nationality = "Thai";
	private String award;

	public ThaiPlayer(String n, int d, int m, int y, double w, double h) {
		super(n, d, m, y, w, h);

	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String newnationality) {
		nationality = newnationality;
	}

	public String getAward() {
		return award;
	}

	public void setaward(String newaward) {
		award = newaward;
	}

	public String getSportName() {
		return award;
	}

	public void setSportName(String newsportName) {
		award = newsportName;
	}

	public String toString() {
		if (award != null)
			return super.toString() + "\n[ nationality = " + nationality
					+ " award = " + award + "]";
		else
			return super.toString() + "\n[ nationality = " + nationality + "]";

	}
}
